# UIBuilder tutorial's Car rental application

In this repository you can find the car rental application the UIBuilder
tutorial describes. Currently, the `master` branch contains the fully 
implemented version of the app. (Later the solutions for each part in the
tutorial will be added as separate branches.)


To build the app run:
```
mvn clean package
```


The application can be started with:
```
mvn clean package spring-boot:run
```