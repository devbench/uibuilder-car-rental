package io.devbench.uibuilder.tutorials.model;

public enum RentStatus {
    ACTIVE("Active"),
    CANCELED("Canceled"),
    CAR_RELEASED("Car released"),
    CAR_RETURNED("Car returned");

    private final String name;

    RentStatus(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}