package io.devbench.uibuilder.tutorials.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.Persistable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.UUID;

@Data
@MappedSuperclass
@EqualsAndHashCode(of = "id")
public abstract class BaseEntity implements Persistable<String>, Serializable {

    @Id
    private String id = UUID.randomUUID().toString();

    @Version
    private Long version;

    @Override
    public boolean isNew() {
        return version == null;
    }
}