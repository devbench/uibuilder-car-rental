package io.devbench.uibuilder.tutorials.model;

public enum CarBodyStyle {
    SEDAN("Sedan"),
    CONVERTIBLE("Convertible"),
    COUPE("Coupe"),
    REGULAR_CAB_PICKUP("Regular Cab Pickup"),
    CREW_CAB_PICKUP("Crew Cab Pickup"),
    EXTENDED_CAB_PICKUP("Extended Cab Pickup"),
    MINIVAN("Minivan"),
    CARGO_VAN("Cargo Van"),
    PASSENGER_VAN("Passenger Van"),
    SUV("SUV"),
    WAGON("Wagon"),
    HATCHBACK("Hatchback");

    final String label;

    CarBodyStyle(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }
}