package io.devbench.uibuilder.tutorials.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
@Table(name = "cars")
@EqualsAndHashCode(callSuper = true)
public class Car extends BaseEntity {

    @NotNull
    @Size(max = 30)
    private String brand;

    @NotNull
    @Size(max = 30)
    private String model;

    @NotNull
    @Digits(integer = 4, fraction = 0)
    private Integer year;

    @NotNull
    @Digits(integer = 8, fraction = 0)
    private Long mileage;

    @NotNull
    @Size(max = 15)
    private String color;

    @Size(max = 100)
    private String description;

    @Column(name = "car_bodystyle")
    @Enumerated(value = EnumType.STRING)
    private CarBodyStyle carBodystyle;

    public Car(String brand, String model, Integer year, Long mileage, String color, String description, CarBodyStyle carBodystyle) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.mileage = mileage;
        this.color = color;
        this.description = description;
        this.carBodystyle = carBodystyle;
    }
}