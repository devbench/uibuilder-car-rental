package io.devbench.uibuilder.tutorials.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@NoArgsConstructor
@Table(name = "rents")
@EqualsAndHashCode(callSuper = true)
public class Rent extends BaseEntity {

    @NotNull
    @Column(name = "rent_status")
    @Enumerated(value = EnumType.STRING)
    private RentStatus rentStatus = RentStatus.ACTIVE;

    @Size(max = 100)
    @Column(name = "return_state_message")
    private String returnStateMessage;

    @OneToOne(optional = false, targetEntity = Request.class)
    @JoinColumn(name = "request_id", unique = true)
    private Request request;

    public Rent(Request request) {
        this.request = request;
    }
}