package io.devbench.uibuilder.tutorials.model;

import io.devbench.uibuilder.tutorials.model.auth.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "requests")
public class Request extends BaseEntity {

    @NotNull
    @Size(max = 30)
    private String title;

    @ManyToOne(optional = false, targetEntity = Car.class)
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "customer_id")
    private User customer;

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "employee_id")
    private User employee;

    @NotNull
    @Column(name = "request_status")
    @Enumerated(value = EnumType.STRING)
    private RequestStatus requestStatus = RequestStatus.WAITING_FOR_RESPONSE;

    @NotNull
    @Column(name = "date_from")
    private LocalDate dateFrom;

    @NotNull
    @Column(name = "date_to")
    private LocalDate dateTo;

    @Size(max = 100)
    private String message;

    @Column(name = "request_date_time")
    private LocalDateTime requestDateTime;

    @Column(name = "decision_date_time")
    private LocalDateTime decisionDateTime;
}