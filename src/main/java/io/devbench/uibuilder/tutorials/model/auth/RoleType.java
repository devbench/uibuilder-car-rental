package io.devbench.uibuilder.tutorials.model.auth;

public enum RoleType {
    CUSTOMER,
    EMPLOYEE
}
