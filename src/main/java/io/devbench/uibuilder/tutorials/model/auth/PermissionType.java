package io.devbench.uibuilder.tutorials.model.auth;

import org.apache.shiro.authz.Permission;

public enum PermissionType implements Permission {
    CAR, SETTINGS, USER_REQUEST, REQUEST_MANAGER, RENT_MASTER, RENT_DETAIL;

    @Override
    public boolean implies(Permission permission) {
        return equals(permission);
    }
}