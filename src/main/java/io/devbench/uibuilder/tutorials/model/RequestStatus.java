package io.devbench.uibuilder.tutorials.model;

public enum RequestStatus {
    WAITING_FOR_RESPONSE {
        @Override
        public String toString() {
            return "Waiting for response";
        }
    },
    ACCEPTED{
        @Override
        public String toString() {
            return "Accepted";
        }
    },
    REJECTED {
        @Override
        public String toString() {
            return "Rejected";
        }
    }
}