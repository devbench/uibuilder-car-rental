package io.devbench.uibuilder.tutorials.model.auth;

import io.devbench.uibuilder.tutorials.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@Table(name = "role_permissions")
@EqualsAndHashCode(callSuper = true)
public class RolePermission extends BaseEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "role_id")
    private UserRole userRole;

    @NotNull
    @Column(name = "permission")
    @Enumerated(value = EnumType.STRING)
    private PermissionType permissionType;

    public RolePermission(UserRole userRole, PermissionType permissionType) {
        this.userRole = userRole;
        this.permissionType = permissionType;
    }
}