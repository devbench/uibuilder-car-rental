package io.devbench.uibuilder.tutorials.model.auth;

import io.devbench.uibuilder.tutorials.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@Table(name = "user_roles")
@EqualsAndHashCode(callSuper = true)
public class UserRole extends BaseEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType;

    public UserRole(User user, RoleType roleType) {
        this.user = user;
        this.roleType = roleType;
    }
}