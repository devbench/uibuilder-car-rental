package io.devbench.uibuilder.tutorials.model.auth;

import io.devbench.uibuilder.tutorials.model.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.util.Strings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "users")
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

    @NotNull
    private String name;

    @NotNull
    @Column(unique = true)
    private String username;

    @NotNull
    private String password;

    @Column(name = "last_login")
    private LocalDateTime lastLogin;

    public User(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = DigestUtils.sha256Hex(password);
    }

    public void setPassword(String password) {
        this.password = DigestUtils.sha256Hex(password);
    }

    public String getPassword() {
        return Strings.EMPTY;
    }
}