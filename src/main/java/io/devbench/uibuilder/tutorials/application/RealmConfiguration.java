package io.devbench.uibuilder.tutorials.application;

import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class RealmConfiguration {

    @Bean
    public JdbcRealm jdbcRealm(DataSource dataSource) {
        JdbcRealm realm = new JdbcRealm();
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        credentialsMatcher.setHashAlgorithmName(Sha256Hash.ALGORITHM_NAME);
        realm.setCredentialsMatcher(credentialsMatcher);
        realm.setCacheManager(new MemoryConstrainedCacheManager());
        realm.setUserRolesQuery("select role from user_roles where user_id = (select id from users where username = ?)");
        realm.setPermissionsQuery("select permission from role_permissions where role_id = (select id from user_roles where role = ?)");
        realm.setDataSource(dataSource);
        realm.setPermissionsLookupEnabled(true);
        realm.init();
        return realm;
    }
}