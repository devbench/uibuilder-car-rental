package io.devbench.uibuilder.tutorials.application;

import io.devbench.uibuilder.tutorials.model.*;
import io.devbench.uibuilder.tutorials.model.auth.*;
import io.devbench.uibuilder.tutorials.repository.RequestRepository;
import io.devbench.uibuilder.tutorials.repository.CarRepository;
import io.devbench.uibuilder.tutorials.repository.RentRepository;
import io.devbench.uibuilder.tutorials.repository.auth.RolePermissionRepository;
import io.devbench.uibuilder.tutorials.repository.auth.UserRepository;
import io.devbench.uibuilder.tutorials.repository.auth.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@Configuration
@RequiredArgsConstructor
public class StartupInitializer {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final RolePermissionRepository rolePermissionRepository;

    private final CarRepository carRepository;
    private final RequestRepository requestRepository;
    private final RentRepository rentRepository;

    @PostConstruct
    void init() {
        User customer = new User("test customer", "customer", "customer");
        customer.setLastLogin(LocalDateTime.now());
        User employee = new User("test employee", "employee", "employee");
        employee.setLastLogin(LocalDateTime.now());
        userRepository.save(customer);
        userRepository.save(employee);

        UserRole customerUserRole = new UserRole(customer, RoleType.CUSTOMER);
        UserRole employeeUserRole = new UserRole(employee, RoleType.EMPLOYEE);
        userRoleRepository.save(customerUserRole);
        userRoleRepository.save(employeeUserRole);

        rolePermissionRepository.saveAll(Arrays.asList(
                new RolePermission(customerUserRole, PermissionType.CAR),
                new RolePermission(customerUserRole, PermissionType.USER_REQUEST),
                new RolePermission(customerUserRole, PermissionType.RENT_MASTER),
                new RolePermission(customerUserRole, PermissionType.RENT_DETAIL),
                new RolePermission(customerUserRole, PermissionType.SETTINGS),

                new RolePermission(employeeUserRole, PermissionType.CAR),
                new RolePermission(employeeUserRole, PermissionType.REQUEST_MANAGER),
                new RolePermission(employeeUserRole, PermissionType.RENT_MASTER),
                new RolePermission(employeeUserRole, PermissionType.RENT_DETAIL),
                new RolePermission(employeeUserRole, PermissionType.SETTINGS)
        ));


        Car car = new Car("Audi", "R8", 2019, (long) 10000, "Blue", "Brand new car.", CarBodyStyle.COUPE);
        carRepository.save(car);

        carRepository.saveAll(Arrays.asList(
                new Car("Citroen", "C3", 2018, (long) 50000, "White", "No description available.", CarBodyStyle.REGULAR_CAB_PICKUP),
                new Car("Toyota", "Sienna", 2019, (long) 195085, "Black", "Automatic 8-Speed", CarBodyStyle.CARGO_VAN),
                new Car("Ford", "Taurus", 2018, (long) 132402, "Gray", "Automatic 6-Speed, Gasoline, Aluminum Wheels", CarBodyStyle.SEDAN),
                new Car("Mazda", "CX-5", 2016, (long) 28873, "Black", "2016 Mazda CX-5 Touring FWD 6-Speed Automatic", CarBodyStyle.SUV),
                new Car("Nissan", "Maxima", 2017, (long) 19409, "Red", "Navigation, Audio Controls on Steering Wheel, Bluetooth, Automatic Climate Control, Good MPG", CarBodyStyle.COUPE)
        ));
        Request request = new Request();
        request.setCar(car);
        request.setCustomer(customer);
        request.setEmployee(employee);
        request.setDateFrom(LocalDate.now());
        request.setDateTo(LocalDate.now());
        request.setTitle("Gimme car");
        request.setRequestStatus(RequestStatus.ACCEPTED);
        request.setRequestDateTime(LocalDateTime.now());
        request.setDecisionDateTime(LocalDateTime.now());
        requestRepository.save(request);

        Rent rent = new Rent(request);
        rentRepository.save(rent);
    }
}