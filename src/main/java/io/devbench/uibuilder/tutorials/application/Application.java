package io.devbench.uibuilder.tutorials.application;

import io.devbench.uibuilder.annotations.EnableUIBuilder;
import io.devbench.uibuilder.annotations.StaticContent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@StaticContent(lookupPaths = "/static")
@EnableUIBuilder("io.devbench.uibuilder.tutorials")
@EntityScan(basePackages = {"io.devbench.uibuilder.tutorials.model"})
@SpringBootApplication(scanBasePackages = {"io.devbench.uibuilder.tutorials"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
