package io.devbench.uibuilder.tutorials.application;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("io.devbench.uibuilder.tutorials")
public class DatabaseConfiguration {
}