package io.devbench.uibuilder.tutorials.service;

import io.devbench.uibuilder.tutorials.model.auth.RoleType;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.auth.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;

    public User getActiveUser() {
        return userRepository.findByUsername(getUsername());
    }

    private Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    private String getUsername() {
        return (String) getSubject().getPrincipal();
    }

    public void logout() {
        getSubject().logout();
    }

    public boolean hasRole(RoleType role) {
        return getSubject().hasRole(role.name());
    }

    public User setUsersLastLogin(User user) {
        user.setLastLogin(LocalDateTime.now());
        return userRepository.save(user);
    }

    public boolean isEmployee() {
        return hasRole(RoleType.EMPLOYEE);
    }
}