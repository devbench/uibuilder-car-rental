package io.devbench.uibuilder.tutorials.service;

import io.devbench.uibuilder.tutorials.model.RequestStatus;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.RequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificationService {

    private final RequestRepository requestRepository;

    public boolean hasNewNotification(User user) {
        return !requestRepository.findAllByCustomer(user).stream()
                .filter(request -> request.getRequestStatus() != RequestStatus.WAITING_FOR_RESPONSE)
                .filter(request -> request.getDecisionDateTime().isAfter(user.getLastLogin()))
                .collect(Collectors.toList()).isEmpty();
    }

}