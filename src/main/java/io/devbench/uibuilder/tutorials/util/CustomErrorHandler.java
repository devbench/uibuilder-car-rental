package io.devbench.uibuilder.tutorials.util;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.server.ErrorEvent;
import com.vaadin.flow.server.ErrorHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CustomErrorHandler implements ErrorHandler {

    @Override
    public void error(ErrorEvent event) {
        Throwable throwable = event.getThrowable();
        String message = throwable.getCause().getMessage();
        log.error(message, throwable);
        showNotification(message);
    }

    private void showNotification(String message) {
        Notification.show(message, 5000, Notification.Position.MIDDLE);
    }
}