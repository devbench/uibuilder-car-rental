package io.devbench.uibuilder.tutorials.util.exception;

public class InvalidIntervalException extends Exception {
    public InvalidIntervalException(String message) {
        super(message);
    }
}