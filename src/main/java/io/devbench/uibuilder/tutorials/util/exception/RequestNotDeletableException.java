package io.devbench.uibuilder.tutorials.util.exception;

public class RequestNotDeletableException extends Exception {
    public RequestNotDeletableException(String message) {
        super(message);
    }
}