package io.devbench.uibuilder.tutorials.util;

import io.devbench.uibuilder.core.controllerbean.StringPropertyConverter;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimePropertyConverter implements StringPropertyConverter<LocalDateTime> {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Override
    public String convertTo(LocalDateTime value) {
        if (value != null) {
            return value.format(dateTimeFormatter);
        }
        return null;
    }

    @Override
    public LocalDateTime apply(@NotNull String value) {
        return LocalDateTime.parse(value, dateTimeFormatter);
    }
}