package io.devbench.uibuilder.tutorials.util.exception;

public class CarNotAssignedException extends Exception {
    public CarNotAssignedException(String message) {
        super(message);
    }
}