package io.devbench.uibuilder.tutorials.repository.customrepository;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.tutorials.model.Car;

public interface CustomCarRepository {
    JPAQuery<Car> queryForAvailableCars();
}