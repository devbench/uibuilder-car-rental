package io.devbench.uibuilder.tutorials.repository.customrepository.implementation;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.tutorials.controllerbean.BaseControllerBean;
import io.devbench.uibuilder.tutorials.model.QRequest;
import io.devbench.uibuilder.tutorials.model.Request;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CustomRequestRepositoryImpl implements CustomRequestRepository {

    @Autowired
    private Provider<BaseControllerBean> baseControllerBean;

    @PersistenceContext
    private EntityManager entityManager;

    private User getCurrentUser() {
        return baseControllerBean.get().getUser();
    }

    @Override
    public JPAQuery<Request> queryForCustomer() {
        JPAQuery<Request> query = new JPAQuery<>(entityManager);
        query.from(QRequest.request)
                .where(QRequest.request.customer.eq(getCurrentUser()));
        return query;
    }

    @Override
    public JPAQuery<Request> queryForEmployees() {
        JPAQuery<Request> query = new JPAQuery<>(entityManager);
        query.from(QRequest.request)
                .where(QRequest.request.employee.eq(getCurrentUser())
                        .or(QRequest.request.employee.isNull()));
        return query;
    }
}