package io.devbench.uibuilder.tutorials.repository.customrepository.implementation;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.tutorials.controllerbean.UserRequestsControllerBean;
import io.devbench.uibuilder.tutorials.model.*;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomCarRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class CustomCarRepositoryImpl implements CustomCarRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private Provider<UserRequestsControllerBean> userRequestsControllerBean;

    private LocalDate getFromDate() {
        return userRequestsControllerBean.get().getFromDate();
    }

    private LocalDate getToDate() {
        return userRequestsControllerBean.get().getToDate();
    }

    @Override
    public JPAQuery<Car> queryForAvailableCars() {
        JPAQuery<Car> query = new JPAQuery<>(entityManager);

        query.from(QCar.car)
                .where(QCar.car.notIn(getNotAvailableCars()));
        return query;
    }

    private Set<Car> getNotAvailableCars() {
        JPAQuery<Request> subQuery = new JPAQuery<>(entityManager);

        if (ObjectUtils.allNotNull(getFromDate(), getToDate())) {
            subQuery.from(QRequest.request)
                    .where((QRequest.request.dateFrom.after(getFromDate())
                            .or(QRequest.request.dateFrom.eq(getFromDate())))
                            .and((QRequest.request.dateTo.before(getToDate())
                                    .or(QRequest.request.dateTo.eq(getToDate()))))
                            .and(QRequest.request.requestStatus.eq(RequestStatus.ACCEPTED)));
            return subQuery.fetch().stream().
                    map(Request::getCar).
                    collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }
}