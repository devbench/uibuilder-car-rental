package io.devbench.uibuilder.tutorials.repository;

import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.tutorials.model.Car;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomCarRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@TargetDataSource(name = "carDS")
public interface CarRepository extends JpaRepository<Car, String>, CustomCarRepository {
}