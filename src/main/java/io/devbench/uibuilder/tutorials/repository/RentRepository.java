package io.devbench.uibuilder.tutorials.repository;

import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.tutorials.model.Rent;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomRentRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@TargetDataSource(name = "rentDS")
public interface RentRepository extends JpaRepository<Rent, String>, CustomRentRepository {
}