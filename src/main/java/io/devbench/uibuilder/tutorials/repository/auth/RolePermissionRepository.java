package io.devbench.uibuilder.tutorials.repository.auth;

import io.devbench.uibuilder.tutorials.model.auth.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, String> {
}