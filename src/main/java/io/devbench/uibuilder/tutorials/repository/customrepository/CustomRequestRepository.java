package io.devbench.uibuilder.tutorials.repository.customrepository;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.tutorials.model.Request;

public interface CustomRequestRepository {
    JPAQuery<Request> queryForCustomer();

    JPAQuery<Request> queryForEmployees();
}