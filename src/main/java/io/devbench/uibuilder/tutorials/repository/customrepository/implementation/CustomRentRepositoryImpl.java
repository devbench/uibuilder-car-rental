package io.devbench.uibuilder.tutorials.repository.customrepository.implementation;

import com.querydsl.jpa.impl.JPAQuery;
import io.devbench.uibuilder.tutorials.controllerbean.BaseControllerBean;
import io.devbench.uibuilder.tutorials.model.QRent;
import io.devbench.uibuilder.tutorials.model.Rent;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomRentRepository;
import io.devbench.uibuilder.tutorials.service.AuthenticationService;
import lombok.RequiredArgsConstructor;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RequiredArgsConstructor
public class CustomRentRepositoryImpl implements CustomRentRepository {

    private final Provider<BaseControllerBean> baseControllerBean;
    private final AuthenticationService authenticationService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public JPAQuery<Rent> queryForUser() {
        JPAQuery<Rent> query = new JPAQuery<>(entityManager);
        if (isEmployee()) {
            query.from(QRent.rent)
                    .where(QRent.rent.request.employee.eq(getCurrentUser()));
        } else {
            query.from(QRent.rent)
                    .where(QRent.rent.request.customer.eq(getCurrentUser()));
        }
        return query;
    }

    private User getCurrentUser() {
        return baseControllerBean.get().getUser();
    }

    private boolean isEmployee() {
        return authenticationService.isEmployee();
    }
}