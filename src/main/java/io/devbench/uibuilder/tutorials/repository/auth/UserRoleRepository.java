package io.devbench.uibuilder.tutorials.repository.auth;

import io.devbench.uibuilder.tutorials.model.auth.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, String> {
}