package io.devbench.uibuilder.tutorials.repository;

import io.devbench.uibuilder.data.api.annotations.TargetDataSource;
import io.devbench.uibuilder.tutorials.model.Request;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.customrepository.CustomRequestRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@TargetDataSource(name = "requestDS")
public interface RequestRepository extends JpaRepository<Request, String>, CustomRequestRepository {

    List<Request> findAllByCustomer(User customer);

}