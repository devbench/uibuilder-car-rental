package io.devbench.uibuilder.tutorials.controllerbean;

import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.repository.auth.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;

@RequiredArgsConstructor
@PageScope("user-settings")
@ControllerBean("userSettingsController")
@RequiresRoles(logical = Logical.OR, value = {"CUSTOMER", "EMPLOYEE"})
public class UserSettingsControllerBean {

    private User user;

    private final UserRepository userRepository;
    private final BaseControllerBean baseControllerBean;

    @UIEventHandler("init")
    public void init(@UIComponent("userForm") UIBuilderForm<User> form) {
        user = baseControllerBean.getUser();
        form.setFormItem(user);
    }

    @UIEventHandler("save")
    public void save(@UIComponent("userForm") UIBuilderForm<User> form) {
        user = userRepository.save(user);
        form.setFormItem(user);
    }
}
