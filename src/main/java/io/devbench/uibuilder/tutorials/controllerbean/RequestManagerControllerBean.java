package io.devbench.uibuilder.tutorials.controllerbean;

import com.vaadin.flow.component.button.Button;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.editorwindow.UIBuilderEditorWindow;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.Car;
import io.devbench.uibuilder.tutorials.model.Rent;
import io.devbench.uibuilder.tutorials.model.Request;
import io.devbench.uibuilder.tutorials.model.RequestStatus;
import io.devbench.uibuilder.tutorials.repository.RentRepository;
import io.devbench.uibuilder.tutorials.repository.RequestRepository;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.time.LocalDateTime;
import java.util.Arrays;

@RequiredArgsConstructor
@RequiresPermissions("REQUEST_MANAGER")
@PageScope("requestManager-page")
@ControllerBean("requestManagerController")
public class RequestManagerControllerBean {

    private final BaseControllerBean baseControllerBean;
    private final RequestRepository requestRepository;
    private final RentRepository rentRepository;

    private UIBuilderGrid<Request> employeeRequestsGrid;
    private UIBuilderMasterDetailController<Request> employeeRequestsMdc;

    @UIEventHandler("init")
    public void init(@UIComponent("rejectButton") Button rejectButton,
                     @UIComponent("acceptButton") Button acceptButton,
                     @UIComponent("employeeRequestsMdc") UIBuilderMasterDetailController<Request> employeeRequestsMdc,
                     @UIComponent("employeeRequestsGrid") UIBuilderGrid<Request> employeeRequestsGrid) {
        this.employeeRequestsMdc = employeeRequestsMdc;
        this.employeeRequestsGrid = employeeRequestsGrid;

        setButtonsEnabled(false, rejectButton, acceptButton);
    }

    private void setButtonsEnabled(boolean enabled, Button... buttons) {
        Arrays.asList(buttons).forEach(button -> button.setEnabled(enabled));
    }

    @UIEventHandler("accept")
    public void accept() {
        save(RequestStatus.ACCEPTED);
        createRentFromRequest();
    }

    private void createRentFromRequest() {
        rentRepository.save(new Rent(getSelectedItem()));
    }

    @UIEventHandler("reject")
    public void reject() {
        save(RequestStatus.REJECTED);
    }

    private void save(RequestStatus status) {
        Request request = getSelectedItem();
        request.setRequestStatus(status);
        request.setDecisionDateTime(LocalDateTime.now());
        request.setEmployee(baseControllerBean.getUser());
        requestRepository.save(request);
        refresh();
        getEditorWindow().close();
    }

    private void refresh() {
        employeeRequestsGrid.deselect();
        employeeRequestsGrid.refresh();
    }

    private Request getSelectedItem() {
        return employeeRequestsMdc.getSelectedItem();
    }

    private UIBuilderEditorWindow<Request> getEditorWindow() {
        return (UIBuilderEditorWindow<Request>) employeeRequestsMdc.getDetailComponent().get();
    }

    @UIEventHandler(value = "onItemSelected", qualifiers = EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET)
    public void onItemSelected(@UIComponent("requestManagerCarForm") UIBuilderForm<Car> carForm,
                               @UIComponent("rejectButton") Button rejectButton,
                               @UIComponent("acceptButton") Button acceptButton) {
        Request request = getSelectedItem();
        if (request == null) {
            carForm.setFormItem(null);
            setButtonsEnabled(false, rejectButton, acceptButton);
        } else {
            carForm.setFormItem(request.getCar());
            if (request.getRequestStatus() == RequestStatus.WAITING_FOR_RESPONSE) {
                setButtonsEnabled(true, rejectButton, acceptButton);
            } else {
                setButtonsEnabled(false, rejectButton, acceptButton);
            }
            getEditorWindow().edit(request);
        }
    }

}
