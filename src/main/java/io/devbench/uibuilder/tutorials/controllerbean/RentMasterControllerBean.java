package io.devbench.uibuilder.tutorials.controllerbean;

import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.Item;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.core.flow.NavigationBuilder;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.Rent;
import org.apache.shiro.authz.annotation.RequiresPermissions;

@PageScope("rentMaster-page")
@RequiresPermissions("RENT_MASTER")
@ControllerBean("rentMasterController")
public class RentMasterControllerBean {

    @UIEventHandler("seeDetails")
    public void seeDetails(@Item Rent rent) {
        NavigationBuilder
                .to("rentDetail")
                .urlParameter("item-id", rent.getId())
                .navigate();
    }
}