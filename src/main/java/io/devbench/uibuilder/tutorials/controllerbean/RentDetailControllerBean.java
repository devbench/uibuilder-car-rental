package io.devbench.uibuilder.tutorials.controllerbean;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.textfield.TextArea;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.core.flow.FlowParameter;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.Car;
import io.devbench.uibuilder.tutorials.model.Rent;
import io.devbench.uibuilder.tutorials.model.RentStatus;
import io.devbench.uibuilder.tutorials.model.Request;
import io.devbench.uibuilder.tutorials.model.auth.RoleType;
import io.devbench.uibuilder.tutorials.repository.RentRepository;
import io.devbench.uibuilder.tutorials.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.time.LocalDate;

@RequiredArgsConstructor
@PageScope("rentDetail-page")
@RequiresPermissions("RENT_DETAIL")
@ControllerBean("rentDetailController")
public class RentDetailControllerBean {

    private final RentRepository rentRepository;
    private final AuthenticationService authenticationService;

    private Rent rent;

    private UIBuilderForm<Rent> rentForm;

    private Button cancelButton;
    private Button releaseButton;
    private Button returnButton;
    private Button messageButton;
    private Button saveButton;


    @UIEventHandler("init")
    public void init(@UIComponent("rentDetailForm") UIBuilderForm<Rent> rentForm,
                     @UIComponent("requestDetailForm") UIBuilderForm<Request> requestForm,
                     @UIComponent("carDetailForm") UIBuilderForm<Car> carForm,
                     @UIComponent("cancelButton") Button cancelButton,
                     @UIComponent("releaseButton") Button releaseButton,
                     @UIComponent("returnButton") Button returnButton,
                     @UIComponent("messageButton") Button messageButton,
                     @UIComponent("saveButton") Button saveButton) {
        this.rentForm = rentForm;
        this.rentForm.setFormItem(rent);
        requestForm.setFormItem(rent.getRequest());
        carForm.setFormItem(rent.getRequest().getCar());

        this.cancelButton = cancelButton;
        this.releaseButton = releaseButton;
        this.returnButton = returnButton;
        this.messageButton = messageButton;
        this.saveButton = saveButton;

        setButtonsVisibility(false);
    }

    @UIEventHandler("beforeLoad")
    public void beforeLoad(@FlowParameter("item-id") String itemId) {
        rentRepository.findById(itemId).ifPresent(rent -> this.rent = rent);
    }

    private void setButtonsVisibility(boolean saveButtonVisibility) {
        setCancelButtonVisible();
        setReleaseButtonVisible();
        setReturnButtonVisible();
        setSaveButtonVisible(saveButtonVisibility);
        setMessageButtonVisible();
    }

    private void setCancelButtonVisible() {
        cancelButton.setVisible(rent.getRequest().getDateFrom().isAfter(LocalDate.now())
                && rent.getRentStatus() == RentStatus.ACTIVE);
    }

    private void setReleaseButtonVisible() {
        releaseButton.setVisible(rent.getRequest().getDateFrom().isEqual(LocalDate.now())
                && authenticationService.hasRole(RoleType.EMPLOYEE)
                && rent.getRentStatus() == RentStatus.ACTIVE);
    }

    private void setReturnButtonVisible() {
        LocalDate endDate = rent.getRequest().getDateTo();
        returnButton.setVisible((endDate.isEqual(LocalDate.now()) || endDate.isBefore(LocalDate.now()))
                && authenticationService.hasRole(RoleType.EMPLOYEE)
                && rent.getRentStatus() == RentStatus.CAR_RELEASED);
    }

    private void setSaveButtonVisible(boolean saveButtonVisibility) {
        saveButton.setVisible(saveButtonVisibility);
    }

    private void setMessageButtonVisible() {
        messageButton.setVisible(authenticationService.hasRole(RoleType.EMPLOYEE)
                && !saveButton.isVisible()
                && rent.getRentStatus() == RentStatus.CAR_RETURNED);
    }

    @UIEventHandler("cancel")
    public void cancel() {
        setRentStatus(RentStatus.CANCELED);
        setButtonsVisibility(false);
    }

    @UIEventHandler("release")
    public void release() {
        setRentStatus(RentStatus.CAR_RELEASED);
        setButtonsVisibility(false);
    }

    @UIEventHandler("return")
    public void returned() {
        setRentStatus(RentStatus.CAR_RETURNED);
        setButtonsVisibility(false);
    }

    @UIEventHandler("message")
    public void message(@UIComponent("messageTextArea") TextArea messageTextArea) {
        messageTextArea.setReadOnly(false);
        setButtonsVisibility(true);
    }

    @UIEventHandler("onSave")
    public void onSave(@UIComponent("messageTextArea") TextArea messageTextArea) {
        messageTextArea.setReadOnly(true);
        save();
        setButtonsVisibility(false);
    }

    private void setRentStatus(RentStatus rentStatus) {
        rent.setRentStatus(rentStatus);
        save();
    }

    public void save() {
        rent = rentRepository.save(rent);
        rentForm.setFormItem(rent);
    }

}
