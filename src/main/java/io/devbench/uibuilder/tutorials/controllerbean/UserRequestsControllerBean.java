package io.devbench.uibuilder.tutorials.controllerbean;

import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.EventQualifier;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.components.form.UIBuilderForm;
import io.devbench.uibuilder.components.grid.UIBuilderGrid;
import io.devbench.uibuilder.components.masterdetail.UIBuilderMasterDetailController;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.Car;
import io.devbench.uibuilder.tutorials.model.Request;
import io.devbench.uibuilder.tutorials.model.RequestStatus;
import io.devbench.uibuilder.tutorials.repository.RequestRepository;
import io.devbench.uibuilder.tutorials.util.exception.CarNotAssignedException;
import io.devbench.uibuilder.tutorials.util.exception.InvalidIntervalException;
import io.devbench.uibuilder.tutorials.util.exception.RequestNotDeletableException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

@RequiredArgsConstructor
@PageScope("userRequest-page")
@RequiresPermissions("USER_REQUEST")
@ControllerBean("userRequestsController")
public class UserRequestsControllerBean {

    private final RequestRepository requestRepository;
    private final BaseControllerBean baseControllerBean;

    private Div carFormContainer;
    private Div carSelectionContainer;
    private Div afterDecisionContainer;

    private DatePicker fromDatePicker;
    private DatePicker toDatePicker;

    private Button searchButton;
    private Button newIntervalButton;

    private UIBuilderGrid<Request> requestsGrid;
    private UIBuilderDetailPanel<Request> userRequestDetailPanel;
    private UIBuilderMasterDetailController<Request> requestMdc;

    public Request createUserRequest() {
        Request request = new Request();
        request.setCustomer(baseControllerBean.getUser());
        return request;
    }

    @UIEventHandler("init")
    public void init(@UIComponent("carFormContainer") Div carFormContainer,
                     @UIComponent("carSelectionContainer") Div carSelectionContainer,
                     @UIComponent("afterDecisionContainer") Div afterDecisionContainer,
                     @UIComponent("fromDatePicker") DatePicker fromDatePicker,
                     @UIComponent("toDatePicker") DatePicker toDatePicker,
                     @UIComponent("searchButton") Button searchButton,
                     @UIComponent("newIntervalButton") Button newIntervalButton,
                     @UIComponent("userRequestsGrid") UIBuilderGrid<Request> requestsGrid,
                     @UIComponent("userRequestDetailPanel") UIBuilderDetailPanel<Request> userRequestDetailPanel,
                     @UIComponent("userRequestsMdc") UIBuilderMasterDetailController<Request> requestMdc)  {
        this.carFormContainer = carFormContainer;
        this.carSelectionContainer = carSelectionContainer;
        this.afterDecisionContainer = afterDecisionContainer;

        this.fromDatePicker = fromDatePicker;
        this.toDatePicker = toDatePicker;

        this.searchButton = searchButton;
        this.newIntervalButton = newIntervalButton;

        this.requestsGrid = requestsGrid;
        this.userRequestDetailPanel = userRequestDetailPanel;
        this.requestMdc = requestMdc;

        searchButton.setEnabled(false);
        newIntervalButton.setEnabled(false);

        setDivsVisibility(false, carFormContainer, carSelectionContainer, afterDecisionContainer);

        fromDatePicker.addValueChangeListener(this::datePickerValueChanged);
        toDatePicker.addValueChangeListener(this::datePickerValueChanged);
    }

    private void setDivsVisibility(boolean visible, Div... divs) {
        Arrays.asList(divs).forEach(div -> div.setVisible(visible));
    }

    private void datePickerValueChanged(ComponentEvent<?> event) {
        if (ObjectUtils.allNotNull(fromDatePicker.getValue(), toDatePicker.getValue())
                && requestMdc.isSelectedItemCreated()) {
            searchButton.setEnabled(true);
        } else {
            searchButton.setEnabled(false);
        }
    }

    @UIEventHandler("onSave")
    public void save() throws CarNotAssignedException {
        Request request = getSelectedItem();
        if (request.getCar() == null) {
            throw new CarNotAssignedException("Car must be assigned to the booking request!");
        }
        request.setRequestDateTime(LocalDateTime.now());
        requestRepository.save(request);
        refresh();
    }

    @UIEventHandler("onDelete")
    public void delete() throws RequestNotDeletableException {
        Request request = getSelectedItem();
        if (request.getRequestStatus() != RequestStatus.WAITING_FOR_RESPONSE) {
            throw new RequestNotDeletableException("The selected request cannot be deleted!");
        }
        requestRepository.delete(request);
        refresh();
    }

    @UIEventHandler("onRefresh")
    public void refresh() {
        requestsGrid.deselect();
        requestsGrid.refresh();
    }

    @UIEventHandler("onCancel")
    public void cancel() {
        getSelectedItem().setCar(null);
        carSelectionContainer.setVisible(false);
        searchButton.setEnabled(false);
        newIntervalButton.setEnabled(false);
        fromDatePicker.setEnabled(true);
        toDatePicker.setEnabled(true);
        fromDatePicker.setValue(null);
        toDatePicker.setValue(null);
    }

    @UIEventHandler(value = "onItemSelected", qualifiers = EventQualifier.MASTER_DETAIL_CONTROLLER_ITEM_SET)
    public void onItemSelected(@UIComponent("carForm") UIBuilderForm<Car> carForm) {
        newIntervalButton.setEnabled(false);
        if (getSelectedItem() == null) {
            setDivsVisibility(false, carFormContainer, carSelectionContainer, afterDecisionContainer);
        } else {
            if (requestMdc.isSelectedItemCreated()) {
                userRequestDetailPanel.setReadonly(false);
                setDivsVisibility(false, carFormContainer, carSelectionContainer, afterDecisionContainer);
            } else {
                carSelectionContainer.setVisible(false);
                setDivsVisibility(true, carFormContainer);
                afterDecisionContainer.setVisible(getSelectedItem().getRequestStatus() != RequestStatus.WAITING_FOR_RESPONSE);
                carForm.setFormItem(getSelectedItem().getCar());
                userRequestDetailPanel.setReadonly(true);
            }
        }
    }

    @UIEventHandler("searchCars")
    public void searchCars(@UIComponent("requestCarsGrid") UIBuilderGrid<Car> requestCarsGrid) throws InvalidIntervalException {
        if (getToDate().isBefore(getFromDate())) {
            throw new InvalidIntervalException("The start date must be before the end date!");
        }
        if (getToDate().isBefore(LocalDate.now()) || getFromDate().isBefore(LocalDate.now())) {
            throw new InvalidIntervalException("The start date and the end date must be after the current day!");
        }
        carSelectionContainer.setVisible(true);
        requestCarsGrid.deselect();
        requestCarsGrid.refresh();
        setSearchFieldsEnabled(false);
    }

    private void setSearchFieldsEnabled(boolean enabled) {
        fromDatePicker.setEnabled(enabled);
        toDatePicker.setEnabled(enabled);
        newIntervalButton.setEnabled(!enabled);
        searchButton.setEnabled(enabled);
    }

    @UIEventHandler("setNewInterval")
    public void setNewInterval() {
        carSelectionContainer.setVisible(false);
        getSelectedItem().setCar(null);
        setSearchFieldsEnabled(true);
    }

    private Request getSelectedItem() {
        return requestMdc.getSelectedItem();
    }

    public LocalDate getFromDate() {
        return fromDatePicker.getValue();
    }

    public LocalDate getToDate() {
        return toDatePicker.getValue();
    }

    @UIEventHandler("onCarSelection")
    public void carSelectionChanged(@UIComponent("requestCarsGrid") UIBuilderGrid<Car> requestCarsGrid) {
        getSelectedItem().setCar(requestCarsGrid.getSelectedItem());
    }
}
