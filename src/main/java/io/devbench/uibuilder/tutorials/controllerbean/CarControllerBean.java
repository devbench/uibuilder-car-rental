package io.devbench.uibuilder.tutorials.controllerbean;

import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.UIComponent;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.components.detailpanel.UIBuilderDetailPanel;
import io.devbench.uibuilder.spring.crud.AbstractSpringControllerBean;
import io.devbench.uibuilder.spring.page.PageScope;
import io.devbench.uibuilder.tutorials.model.Car;
import io.devbench.uibuilder.tutorials.model.CarBodyStyle;
import io.devbench.uibuilder.tutorials.model.auth.RoleType;
import io.devbench.uibuilder.tutorials.repository.CarRepository;
import io.devbench.uibuilder.tutorials.service.AuthenticationService;
import lombok.Getter;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.util.Arrays;
import java.util.List;

@PageScope("car-page")
@RequiresPermissions("CAR")
@ControllerBean("carController")
public class CarControllerBean extends AbstractSpringControllerBean<Car, CarRepository> {

    private final AuthenticationService authenticationService;

    @Getter
    private List<CarBodyStyle> carBodystyleList = Arrays.asList(CarBodyStyle.values());

    public CarControllerBean(CarRepository repository, AuthenticationService authenticationService) {
        super(Car::new, repository);
        this.authenticationService = authenticationService;
    }

    @UIEventHandler("init")
    public void init(@UIComponent("carDetailPanel") UIBuilderDetailPanel<Car> carDetailPanel) {
        carDetailPanel.setReadonly(!authenticationService.hasRole(RoleType.EMPLOYEE));
    }
}
