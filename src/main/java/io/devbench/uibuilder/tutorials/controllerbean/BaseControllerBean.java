package io.devbench.uibuilder.tutorials.controllerbean;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import io.devbench.uibuilder.annotations.ControllerBean;
import io.devbench.uibuilder.api.controllerbean.uieventhandler.UIEventHandler;
import io.devbench.uibuilder.tutorials.model.auth.RoleType;
import io.devbench.uibuilder.tutorials.model.auth.User;
import io.devbench.uibuilder.tutorials.service.AuthenticationService;
import io.devbench.uibuilder.tutorials.service.NotificationService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@RequiredArgsConstructor
@ControllerBean("baseController")
public class BaseControllerBean {

    private final AuthenticationService authenticationService;
    private final NotificationService notificationService;

    @Getter
    private User user;


    @UIEventHandler("init")
    public void init() {
        user = authenticationService.getActiveUser();
        if (authenticationService.hasRole(RoleType.CUSTOMER) && notificationService.hasNewNotification(user)) {
            Notification.show("Your request has been judged!", 5000, Notification.Position.MIDDLE);
        }
        user = authenticationService.setUsersLastLogin(user);
    }

    public String getName() {
        return user == null ? "" : user.getName();
    }

    @UIEventHandler("logout")
    public void logout() {
        authenticationService.logout();
        UI.getCurrent().getPage().reload();
        user = null;
    }
}